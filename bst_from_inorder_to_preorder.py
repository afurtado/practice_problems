class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

class Tree:
    def construct_binary_tree(self, preorder, inorder):
        if len(preorder)==0:
            return
        if len(preorder)==1:
            return Node(preorder[0])
        root=Node(preorder[0])
        index=inorder.index(preorder[0])
        root.left=self.construct_binary_tree(
        preorder[1:(index+1)], inorder[0:index])
        root.right=self.construct_binary_tree(
        preorder[(index+1):], inorder[(index+1):])
        return root

preorder=[8, 19, 40, 15, 7]
inorder=[19, 8, 15, 40, 7]
result=Tree().construct_binary_tree(preorder,inorder)
print(result.data)
print(result.left.data)
print(result.right.data)
